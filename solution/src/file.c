#include "file.h"
#include <stdio.h>



bool file_close(FILE** file) {
    if (!*file)
        return false;
    return fclose(* file);
}
