#include "bmp.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>

#define BMP_TYPE 0x4d42
#define size 40
#define planes 1
#define count 24

uint8_t padding_make(uint64_t width){
    return 3 - (width * 3 + 3) % 4;
}

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static struct bmp_header create (const uint64_t width, const uint64_t height) {
    struct bmp_header header = {
            .bfType = BMP_TYPE,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = size,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = planes,
            .biBitCount = count,
            .biCompression = 0,
            .biSizeImage =  width * height * sizeof(struct pixel) + height * padding_make(width),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
    header.bfileSize = header.biSizeImage + header.bOffBits;
    return header;
}

static enum read_status check (const struct bmp_header header) {
    if (header.bfType != 0x4d42)
        return READ_INVALID_SIGNATURE;
    if (header.biSize != 40)
        return READ_INVALID_HEADER_SIZE;
    if (header.biPlanes != 1)
        return READ_INVALID_PLANES;
    if (header.biBitCount != 24) {
        return READ_INVALID_BIT_COUNT;
    }
    return READ_OK;
}

enum read_status from_bmp (FILE* in, struct image* img) {
    struct bmp_header header;
    fread(&header, sizeof(struct bmp_header), 1, in);
    enum read_status rs = check(header);
    if ( rs != READ_OK)
        return rs;
    *img = create_img();

    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data =  malloc(img->width * img->height * sizeof(struct pixel));

    uint8_t padding = padding_make(img->width);
    for (uint64_t i = 0; i < img->height; i++) {
        fread(&(img->data[i*(img->width)]), sizeof(struct pixel), img->width, in);
        fseek(in, padding, SEEK_CUR);
    }
    return READ_OK;
}

const char* const err_read[] = {
        [READ_OK] = "Successful reading\n",
        [READ_INVALID_SIGNATURE] = "Wrong signature\n",
        [READ_INVALID_HEADER_SIZE] = "Wrong header size\n",
        [READ_INVALID_PLANES] = "Wrong number of planes\n",
        [READ_INVALID_BIT_COUNT] = "Wrong number of bits per pixel\n",

};

void print_err_read (enum read_status error) {
    printf("%s", err_read[error]);
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header = create(img->width, img->height);
    uint8_t padding = padding_make(img->width );
    size_t status = fwrite( &header, 1, sizeof(struct bmp_header), out );
    if( status != sizeof(struct bmp_header) )
        return WRITE_ERROR;
    const uint8_t nulls[] = {0,0,0};
    for(uint64_t i = 0; i < img->height; i++) {
        if(!fwrite(&(img->data[i * img->width]), sizeof(struct pixel), img->width, out) ||
                (padding && !fwrite(nulls, 1, padding, out)))
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

const char* const err_write[] = {
        [WRITE_OK] = "Successful writing\n",
        [WRITE_ERROR] = "Wrong writing\n"
};

void print_err_write (enum write_status error) {
    printf("%s", err_write[error]);
}
