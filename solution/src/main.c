#include "bmp.h"
#include "file.h"
#include "image.h"
#include "rotate.h"
#include <stdio.h>


int main(int argc, char **argv) {
    if (argc != 3) {
        printf("Wrong args");
        return 1;
    }

    FILE *input = fopen(argv[1], "rb");
    FILE *output = fopen(argv[2], "wb");


    struct image old = create_img();

    if (from_bmp(input, &old)) {
        printf("Wrong writing open");
        free_img(&old);
        return 4;
    }

    struct image new = rotate(old);

    if (to_bmp(output, &new)) {
        free_img(&new);
        free_img(&old);
        return 5;
    }

    if (file_close(&input)) {
        free_img(&new);
        free_img(&old);
        return 6;
    }

    if (file_close(&output)){
        printf("Wrong writing close");
        free_img(&new);
        free_img(&old);
        return 7;
    }


    free_img(&new);
    free_img(&old);
    return 0;
}
