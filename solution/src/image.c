#include "image.h"
#include <stdint.h>
#include <stdlib.h>


struct image create_img() {
    return (struct image) {0};
}

static inline uint64_t img_coord(struct image* img, uint64_t j, uint64_t i) {
    return img->width * i + j;
}

struct pixel get_pixel(struct image* img, uint64_t j, uint64_t i) {
    return img->data[img_coord(img, j, i)];
}

void set_pixel(struct image* img, struct pixel pixel, uint64_t j, uint64_t i) {
    img->data[img_coord(img, j, i)] = pixel;
}

void free_img(struct image* img) {
    free(img->data);
    img->data = NULL;
}
