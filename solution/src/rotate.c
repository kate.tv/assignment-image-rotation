#include "rotate.h"
#include "image.h"
#include <stddef.h>


struct image rotate(struct image source) {
    struct image rotate_img = create_img();
    rotate_img.height = source.width;
    rotate_img.width = source.height;
    rotate_img.data =  malloc(rotate_img.width * rotate_img.height * sizeof(struct pixel));

    for (int64_t i =0 ; i < source.height ; i++){
        for (uint64_t j = 0; j < source.width; j++) {
            set_pixel(&rotate_img, get_pixel(&source, j, i), source.height - 1 - i, j);
        }
    }
    return rotate_img;

}
