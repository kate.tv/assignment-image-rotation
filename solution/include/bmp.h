#pragma once
#include <stdint.h>
#include <stdbool.h>
#include "file.h"
#include "image.h"


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_HEADER_SIZE,
    READ_INVALID_PLANES,
    READ_INVALID_BIT_COUNT
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
};
enum read_status from_bmp( FILE* in, struct image* img );
void print_err_read (enum read_status error);
enum write_status to_bmp( FILE* out, struct image const* img );
void print_err_write (enum write_status error);
