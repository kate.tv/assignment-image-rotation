#pragma once
#include  <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_img ();
struct pixel get_pixel(struct image* img, uint64_t j, uint64_t i);
void set_pixel(struct image* img, struct pixel pixel, uint64_t j, uint64_t i);
void free_img (struct image* img);
